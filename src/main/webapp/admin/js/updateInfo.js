$(function() {

	$newnickname = $("#newnickname");
	$newoldpass = $("#newoldpass");
	$newpass = $("#newpass");
	$updatebtn = $("#updatebtn");
	$newrepass = $("#newrepass");
	$updatebtn.click(update);

	function update() {
		newpass = $newpass.val();
		newoldpass = $newoldpass.val();
		newnickname = $newnickname.val();
		newrepass = $newrepass.val();
		if (newpass != newrepass) {
			alert("两次密码不一致");
			return;
		}
		$.ajax({
			url: "/user/update",
			data: "nickname="+newnickname+"&oldpass="+newoldpass+"&password="+newpass,
			dataType: 'json',
			type: 'post',
			success:function(result) {
				alert('update::'+result.info);
				if (result.code == 0) {
					initInfo();
				}
			},
			error:function(result) {
				alert("服务器异常，请稍后再试");
			}
		});
	}
	
	function initInfo() {
		$.ajax({
			url: "/user/getCurrentUser",
			dataType: 'json',
			type: 'post',
			success:function(result) {
				$newnickname.val(result.nickname);
				$("#newusername").val(result.username);
				$("#newuserid").val(result.userid);
			},
			error:function(result) {
				alert("服务器异常，请稍后再试");
			}
		});
	}
	initInfo();
});