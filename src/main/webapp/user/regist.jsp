<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>异清轩博客</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/nprogress.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="apple-touch-icon-precomposed" href="images/icon/icon.png">
<link rel="shortcut icon" href="images/icon/favicon.ico">
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/nprogress.js"></script>
<script src="js/jquery.lazyload.min.js"></script>

<link rel="stylesheet" href="css/regist.css">
<!--[if gte IE 9]>
  <script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
  <script src="js/html5shiv.min.js" type="text/javascript"></script>
  <script src="js/respond.min.js" type="text/javascript"></script>
  <script src="js/selectivizr-min.js" type="text/javascript"></script>
<![endif]-->
<!--[if lt IE 9]>
  <script>window.location.href='upgrade-browser.html';</script>
<![endif]-->
</head>
<body class="user-select" style="height: 100%;">
	<header class="header">
		<nav class="navbar navbar-default" id="navbar">
			<div class="container">
				<div class="header-topbar hidden-xs link-border">
					<ul class="site-nav topmenu">
						<li><a href="tags.html">标签云</a></li>
						<li><a href="readers.html" rel="nofollow">读者墙</a></li>
						<li><a href="links.html" rel="nofollow">友情链接</a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" rel="nofollow">
								关注本站 <span class="caret"></span>
							</a>
							<ul class="dropdown-menu header-topbar-dropdown-menu">
								<li><a data-toggle="modal" data-target="#WeChat" rel="nofollow"><i class="fa fa-weixin"></i> 微信</a></li>
								<li><a href="#" rel="nofollow"><i class="fa fa-weibo"></i> 微博</a></li>
								<li><a data-toggle="modal" data-target="#areDeveloping" rel="nofollow"><i class="fa fa-rss"></i> RSS</a></li>
							</ul>
						</li>
					</ul>
					&nbsp;&nbsp;
				</div>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar" aria-expanded="false"> <span class="sr-only"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
					<h1 class="logo hvr-bounce-in"><a href="" title=""><img src="images/logo.png" alt=""></a></h1>
				</div>
				<div class="collapse navbar-collapse" id="header-navbar">
					<ul class="nav navbar-nav navbar-right" id="headerNav">
						<li class="hidden-index active"><a data-cont="异清轩首页" href="index.html">异清轩首页</a></li>
						<li><a href="category.html">前端技术</a></li>
						<li><a href="category.html">后端程序</a></li>
						<li><a href="category.html">管理系统</a></li>
						<li><a href="category.html">授人以渔</a></li>
						<li><a href="category.html">程序人生</a></li>
						<li><a data-toggle="modal" data-target="#loginModal" class="login" rel="nofollow">登录/注册</a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								admin
								<span style="border: 1px solid black; border-radius: 50px; padding: 5px; " class=" glyphicon glyphicon-user"></span>
								<!-- <span class="caret"></span> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-left">
								<li><a title="查看或修改个人信息" href="../admin/main.html">个人信息</a></li>
								<li><a title="查看您的登录记录" data-toggle="modal" data-target="#seeUserLoginlog">登录记录</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<main class="container register">
		<div class="row clearfix">
			<div class="col-md-4 col-md-offset-4 column">
				<div class="form-box" >
					<main>
					<form class="form-horizontal" id="regform">
						<div class="form-group">
							<h3>账户注册</h3>
						</div>
					<br>
					<div id="reg1">
						<div class="form-group">
							<input type="email" placeholder="邮箱" class="form-control input-lg" name="email" id="email" />
							<label class="control-label" for="email">&nbsp;</label>
						</div>
						<div class="form-group">
							<input type="text" placeholder="验证码" class="form-control input-lg" name="code" id="code" />
							<label class="control-label" for="code">&nbsp;</label>
							<span aria-hidden="true">
								<a id="getCode">获取验证码</a>&nbsp;&nbsp;&nbsp;
							</span>
						</div>
						<div class="form-group">
							<button id="goinfo" class="btn btn-lg btn-default btn-search btn-reg" name="search" type="submit">
								下一步
							</button>
						</div>
					</div>
					<div id="reg2" class="invisible">
						<div class="form-group">
							<input required="required" type="text" placeholder="昵称" class="form-control input-lg" name="nickname" id="nickName" />
							<label class="control-label" for="nickName">&nbsp;</label>
						</div>
						<div class="form-group">
							<input required="required" type="userName" placeholder="用户名" class="form-control input-lg" name="username" id="userName" />
							<label class="control-label" for="userName">&nbsp;</label>
						</div>
						<div class="form-group">
							<input required="required" type="password" placeholder="密码" class="form-control input-lg" name="password" id="password" />
							<label class="control-label" for="password">&nbsp;</label>
						</div>
						<div class="form-group">
							<input required="required" type="password" placeholder="确认密码" class="form-control input-lg" name="repass" id="repass" />
							<label class="control-label" for="repass">&nbsp;</label>
						</div>
						<div class="form-group">
							<button class="btn btn-lg btn-default btn-search btn-reg" id="registbtn" name="search" type="button">
								同意协议并注册
							</button>>
						</div>
					</div>
					<div>
					</div>
					<br>
					<br>
					<a data-toggle="modal" data-target="#loginModal" class="login" rel="nofollow">已有账号？去登录</a>
					</form>
					</main>
				</div>
			</div>
		</div>
	</main>
	<footer class="footer">
		<div class="container">
			<p>&copy; 2016 <a href="">ylsat.com</a> &nbsp; <a href="#" target="_blank" rel="nofollow">豫ICP备20151109-1</a> &nbsp; &nbsp; <a href="http://www.mycodes.net/" target="_blank">源码之家</a></p>
		</div>
		<div id="gotop"><a class="gotop"></a></div>
	</footer>


	<!-- 登录弹出框 -->
	<div class="modal fade user-select" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<form>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="loginModalLabel">登录</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="loginModalUserNmae">用户名</label>
					<input type="text" class="form-control" name="username" id="loginUsername" placeholder="请输入用户名" autofocus maxlength="15" autocomplete="off" required>
				</div>
			<div class="form-group">
				<label for="loginModalUserPwd">密码</label>
				<input type="password" class="form-control" name="password" id="loginpassword" placeholder="请输入密码" maxlength="18" autocomplete="off" required>
			</div>
			<div>
				<a href="/user/regist">没有账号？注册一个</a>
				<a style="float: right;" href="/user/findPass">忘记密码</a>
			</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="loginbtn">登录</button>
			</div>
			</form>
			</div>
		</div>
	</div>

	<!-- 按钮触发模态框 -->
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="infoModalLabel">提示</h4>
				</div>
				<div class="modal-body" id="infoshow"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">确认</button>
				</div>
			</div>
		</div>
	</div>

	<script src="js/bootstrap.min.js"></script> 
	<script src="js/jquery.ias.js"></script> 
	<script src="js/scripts.js"></script>

	<script src="js/regist.js"></script>
	<script src="js/public.js"></script>
</body>
</html>