function logintest() {
	var islogin;
	$.ajax({
		async: false,
		url: "/user/getCurrentUser",
		dataType: 'json',
		type: 'post',
		success:function(result) {
			if ("nickname" in result) {
				islogin = true;
			}else{
				islogin = false;
				$('#loginModal').modal();
			}
		},
		error:function(result) {
			islogin = false;
			$('#loginModal').modal();
		}
	});
	return islogin;
}
$(function() {
	$("#loginbtn").click(function () {
		$.ajax({
			async: false,
			url: "/user/login",
			data: "username="+$("#loginUsername").val()+"&password="+$("#loginpassword").val(),
			dataType: 'json',
			type: 'post',
			success:function(result) {
				if (result.code == 0) {
					location.reload();
				}else{
					alert(result.info);
				}
			},
			error:function(result) {
				alert("服务器异常，请稍后再试");
			}
		});
	});
	$headerNav = $("#headerNav");
	function initCategory(){
		$headerNav.empty();
		$headerNav.append('<li class="hidden-index active"><a data-cont="异清轩首页" href="index.html">异清轩首页</a></li>');
		$.ajax({
			async: false,
			url: "/category/getAll",
			dataType: 'json',
			type: 'post',
			success:function(result) {
				result.forEach(element => {
					$headerNav.append('<li><a href="category.html">'+ element.name + '</a></li>');
				});
			},
			error:function(result) {
				// alert("服务器异常，请稍后再试");
			}
		});
		$.ajax({
			url: "/user/getCurrentUser",
			dataType: 'json',
			type: 'post',
			success:function(result) {
				if ("nickname" in result) {
					$headerNav.append('<li class="dropdown">\
					<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
						'+result.nickname+'\
						<span style="border: 1px solid black; border-radius: 50px; padding: 5px; " class=" glyphicon glyphicon-user"></span>\
						<!-- <span class="caret"></span> -->\
					</a>\
					<ul class="dropdown-menu dropdown-menu-left">\
						<li><a title="查看或修改个人信息" href="/user/center">个人主页</a></li>\
						<li class="divider" role="separator"></li>\
						<li><a href="/user/logout">退出登录</a></li>\
					</ul></li>');
				}else{
					$headerNav.append('<li><a data-toggle="modal" data-target="#loginModal" class="login" rel="nofollow">登录/注册</a></li>');
				}
			},
			error:function(result) {
				$headerNav.append('<li><a data-toggle="modal" data-target="#loginModal" class="login" rel="nofollow">登录/注册</a></li>');
			}
		});
	}
	initCategory();

	$expertbtn = $("#expertbtn");
	$expertbtn.click(function() {
		// 未登录返回
		if (!logintest()) {
			return;
		}

		// $headerNav.append('<li><a data-toggle="modal" data-target="#loginModal" class="login" rel="nofollow">登录/注册</a></li>');
		$.ajax({
			url: "/expert/apply?category=1",
			dataType: 'json',
			type: 'post',
			success:function(result) {
				if (result.code == 0) {
					$expertbtn.removeClass("btn-search");
					$expertbtn.addClass("btn-searchHover");
					$expertbtn.html("申请中 ...")
				}
			},
			error:function(result) {
				// alert("服务器异常，请稍后再试");
			}
		})
		$expertbtn.blur();
	});
});