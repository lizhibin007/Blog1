$(function() {

		// 异步信息配置
		var emailAsyncConf = {
			nullval: "邮箱不能为空",
			success: "邮箱可以使用",
			email: "邮箱格式不正确",
	
			remote: {
				async: true,
				url: "/user/testEmailAvailable",
				data: function() {
					return "email="+$("#email").val()
				},
				dataType: "json",
				type: "post"
			}
		};
		// 同步信息配置
		var emailConf = {
			nullval: "邮箱不能为空",
			success: "邮箱可以使用",
			email: "邮箱格式不正确",
	
			remote: {
				async: false,
				url: "/user/testEmailAvailable",
				data: function() {
					return "email="+$("#email").val()
				},
				dataType: "json",
				type: "post"
			}
		};
	
		// 验证码配置信息
		var codeConf = {
			nullval: "验证码不能为空",
			success: "验证码正确",
			remote: {
				async: false,
				url: "/email/testEmailCode",
				data: function() {
					return "email="+$("#email").val()+"&code="+$("#code").val();
				},
				dataType: "json",
				type: "post"
			}
		}
		var nickNameConf = {
			nullval: "昵称不能为空",
			success: "昵称可以使用",
			maxlength: {
				length: 12,
				message: "昵称最大长度为12"
			}
		}
		// 用户名异步配置
		var userNameAsyncConf = {
			nullval: "用户名不能为空",
			success: "用户名可以使用",
			minlength: {
				length: 8,
				message: "用户名最小长度8"
			},
			maxlength: {
				length: 20,
				message: "用户名最大长度20"
			},
			username: "用户名只能有字母和数字组成，须以字母开头",
			remote: {
				async: true,
				url: "/user/testUsernameAvailable",
				data: function() {
					return "username="+$("#userName").val();
				},
				dataType: "json",
				type: "post"
			}
		}
		// 用户名同步配置
		var userNameConf = {
			nullval: "用户名不能为空",
			success: "用户名可以使用",
			minlength: {
				length: 8,
				message: "用户名最小长度8"
			},
			maxlength: {
				length: 20,
				message: "用户名最大长度20"
			},
			username: "用户名只能有字母和数字组成，须以字母开头",
			remote: {
				async: false,
				url: "/user/testUsernameAvailable",
				data: function() {
					return "username="+$("#userName").val();
				},
				dataType: "json",
				type: "post"
			}
		}
		var passwordConf = {
			nullval: "密码不能为空",
			success: "密码可以使用",
			minlength: {
				length: 8,
				message: "密码最小长度8"
			},
			maxlength: {
				length: 20,
				message: "密码最大长度20"
			}
		}
		var repassConf = {
			equal: {
				to: "password",
				message: "两次密码不一致"
			},
			success: "密码一致"
		}
		
	// 显示文本框提示信息
	function show($ele,cssname,info) {
		$ele.parent().removeClass("has-success");
		$ele.parent().removeClass("has-error");
		$ele.parent().addClass(cssname);
		$ele.next().html(info);
	}

	// 传入文本框以及规则对应的提示，对文本框进行内容验证并显示信息
	// 返回一个验证函数
	function test($myele, myparams) {
		return function() {
			var errorcss = "has-error";
			$ele = $myele;
			params = myparams;
			var eleval = $ele.val();
			// 空值判断
			if ("nullval" in myparams) {
				if(eleval==""){
					show($ele, errorcss, params.nullval);
					return false;
				}
			}
			// 邮箱判断
			if ("email" in params) {
				var reg=/^\w+@([a-zA-Z0-9]{2,10}\.){1,5}([a-z]{2,10})$/;
				if (!reg.test(eleval)) {
					show($ele, errorcss, params.email);
					return false;
				}
			}
			if ("minlength" in params) {
				if (eleval.length < params.minlength.length) {
					show($ele, errorcss, params.minlength.message);
					return false;
				}
					// /^[0-9]{8}$/
		// 
			}
			if ("maxlength" in params) {
				if (eleval.length > params.maxlength.length) {
					show($ele, errorcss, params.maxlength.message);
					return false;
				}
			}
			if ("username" in params){
				var reg = /^[a-zA-Z]+[a-zA-Z0-9]+$/
				if (!reg.test(eleval)) {
					show($ele, errorcss, params.username);
					return false;
				}
			}
			if ("equal" in params) {
				if(eleval != $("#"+params.equal.to).val()){
					show($ele, errorcss, params.equal.message);
					show($("#"+params.equal.to),errorcss,params.equal.message);
					return false;
				}else{
					show($("#"+params.equal.to),"has-success",params.success);
				}
			}
			
			// 远程判断
			if ("remote" in params) {
				var ret = true;
				$.ajax({
					// 设置同步或者异步
					async: params.remote.async,
					url: params.remote.url,
					data: params.remote.data(),
					dataType: params.remote.dataType,
					type: params.remote.type,
					success:function(result) {
						if (result.code != 0) {
							show($ele, errorcss, result.info);
							ret = false;
						}
					},
					error:function(result) {
						show($ele, errorcss, "服务器异常，请稍后再试");
						ret = false;
					}
				});
				if (ret == false) {
					return false;
				}
			}
			
			if ("success" in myparams) {
				show($ele,"has-success",params.success);
			}else{
				show($ele,"has-success","输入正确");
			}
			return true;
		}
	}

	// 邮箱文本框jQuery元素
	var $email = $("#email");
	$email.blur(test($email,emailAsyncConf));
	
	// 验证码文本框jQuery元素
	var $code = $("#code");
	$code.blur(test($code,codeConf));

	// 获取验证码
	var $getCode = $("#getCode");
	$getCode.click(getRegCode);

	// 获取昵称
	var $nickName = $("#nickName");
	$nickName.blur(test($nickName, nickNameConf));

	// 用户名
	var $userName = $("#userName");
	$userName.blur(test($userName, userNameAsyncConf));

	// 密码
	var $password = $("#password");
	$password.blur(test($password, passwordConf));

	// 确认密码
	var $repass = $("#repass");
	$repass.blur(test($repass, repassConf));

	// 下一步
	var $goinfo = $("#goinfo");
	$goinfo.click(goinfo);

	$("#registbtn").click(function(){
		if(!(test($nickName, nickNameConf)())){
			return false;
		}
		if(!(test($userName, userNameConf)())){
			return false;
		}
		if(!(test($password, passwordConf)())){
			return false;
		}
		if(!(test($repass, repassConf)())){
			return false;
		}
		// 提交注册
		$.ajax({
			async: false,
			url: "/user/add",
			data: "email="+$email.val()+"&code="+$code.val()+"&nickname="+$nickName.val()+"&username="+$userName.val()+"&password="+$password.val(),
			dataType: 'json',
			type: 'post',
			success:function(result) {
				if (result.code == 0) {
					var options = {
						"backdrop" : false
					}
					$("#infoshow").html(result.info);
					$('#infoModal').modal(options);
				}
			},
			error:function(result) {
				alert("服务器异常，请稍后再试");
			}
		})
	});

	$('#infoModal').on('hidden.bs.modal', function (e) {
		location.href="/user/index";
	});


	// 获取验证码计时
	var istime = true;
	// 下一步
	function goinfo() {
		if(!(test($email, emailConf)())){
			// 邮箱同步
		}else if(!(test($code,codeConf)())){
			// 验证码异步
		}else{
			// 通过验证
			$("#reg1").addClass("invisible");
			$("#reg2").removeClass("invisible")
			$("#reg2").addClass("visible")
		}
	};
	// 获取验证码
	function getRegCode() {
		if (!istime) {
			alert("请稍后再试");
			return;
		}
		if(!(test($("#email"),emailConf)())){
			// 邮箱同步
			return;
		}
		
		// 获取邮箱
		var email = $("#email").val();
		// 设置倒计时
		var seconds = 60;
		var timer = setInterval(changeTime,1000);
		istime = false;
		changeTime();
		// 防君子
		function changeTime() {
			if(seconds > 0){
				$("#getCode").html(""+seconds+"S后重新获取");
				seconds--;
			}else{
				clearInterval(timer);
				istime = true;
				$("#getCode").html("重新获取");
			}
		}
		// 获取验证码
		$.ajax({
			url: "/email/getRegCode",
			data: "email="+email,
			dataType: 'json',
			type: 'post',
			success:function(result) {
				// alert("获取验证码成功");
			},
			error:function(result) {
				alert("服务器异常，请稍后再试");
			}
		})
	}
});