$(function() {
	$followbtn = $("#followbtn");
	$followbtn.click(function() {
		// 未登录返回
		if (!logintest()) {
			return;
		}
		if ($followbtn.attr("follow") == 'true') {
			$.ajax({
				url: "/follow/cancel?userId="+$followbtn.attr("bloger"),
				dataType: 'json',
				type: 'post',
				success:function(result) {
					if (result.code == 0) {
						// alert(result.info)
						initFollowbtn();
					}
					else{
						alert(result.info);
					}
				},
				error:function(result) {
					alert("服务器异常");
				}
			})
		}else{
			$.ajax({
				url: "/follow/add?userId="+$followbtn.attr("bloger"),
				dataType: 'json',
				type: 'post',
				success:function(result) {
					if (result.code == 0) {
						initFollowbtn();
					}
					else{
						alert(result.info);
					}
				},
				error:function(result) {
					alert("服务器异常");
				}
			})
		}
		$followbtn.blur();
	})
	function initFollowbtn(){
		$.ajax({
			url: "/follow/getStatus?userId="+$followbtn.attr("bloger"),
			dataType: 'json',
			type: 'post',
			success:function(result) {
				$followbtn.attr("follow", result);
				// alert($followbtn.attr("follow"))
				if ($followbtn.attr("follow") == 'true') {
					// alert("true:::"+$followbtn.attr("follow"))
					$followbtn.removeClass("btn-search");
					$followbtn.addClass("btn-searchHover");
					$followbtn.html("已关注")
				}else{
					// alert("false:::" + $followbtn.attr("follow"))
					$followbtn.removeClass("btn-searchHover");
					$followbtn.addClass("btn-search");
					$followbtn.html("关注博主");
				}
			},
			error:function(result) {
				$followbtn.attr("follow", false);
			}
		});
		$followbtn.blur();
	}
	$followbtn.mouseover(function() {
		if ($followbtn.attr("follow") == 'true') {
			$followbtn.html("取消关注");
		}
	});
	$followbtn.mouseout(function() {
		if ($followbtn.attr("follow") == 'true') {
			$followbtn.html("已关注");
		}
	});
	initFollowbtn();
});