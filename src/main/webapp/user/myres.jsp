<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 11327
  Date: 2021/3/29
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>资源上传</title>
    <link rel="stylesheet" type="text/css" href="/user/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/user/css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="/user/css/style.css">
    <link rel="stylesheet" type="text/css" href="/user/css/font-awesome.min.css">
    <link rel="apple-touch-icon-precomposed" href="/user/images/icon/icon.png">
    <link rel="shortcut icon" href="/user/images/icon/favicon.ico">
    <link type="text/css" rel="stylesheet" href="/user/css/upload_res.css">
    <script src="/user/js/jquery-2.1.4.min.js"></script>
    <script src="/user/js/nprogress.js"></script>
    <script src="/user/js/jquery.lazyload.min.js"></script>
    <!--[if gte IE 9]>
    <script src="/user/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/user/js/html5shiv.min.js" type="text/javascript"></script>
    <script src="/user/js/respond.min.js" type="text/javascript"></script>
    <script src="/user/js/selectivizr-min.js" type="text/javascript"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script>window.location.href = '/upgrade-browser.html';</script>
    <![endif]-->
</head>
<body class="user-select">
<header class="header">
    <nav class="navbar navbar-default" id="navbar">
        <div class="container">
            <div class="header-topbar hidden-xs link-border">
                <ul class="site-nav topmenu">
                    <li><a href="tags.html">标签云</a></li>
                    <li><a href="readers.html" rel="nofollow">读者墙</a></li>
                    <li><a href="links.html" rel="nofollow">友情链接</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false" rel="nofollow">关注本站 <span
                            class="caret"></span></a>
                        <ul class="dropdown-menu header-topbar-dropdown-menu">
                            <li><a data-toggle="modal" data-target="#WeChat" rel="nofollow"><i class="fa fa-weixin"></i>
                                微信</a></li>
                            <li><a href="#" rel="nofollow"><i class="fa fa-weibo"></i> 微博</a></li>
                            <li><a data-toggle="modal" data-target="#areDeveloping" rel="nofollow"><i
                                    class="fa fa-rss"></i> RSS</a></li>
                        </ul>
                    </li>
                </ul>
                <a data-toggle="modal" data-target="#loginModal" class="login" rel="nofollow">Hi,请登录</a>&nbsp;&nbsp;<a
                    href="javascript:;" class="register" rel="nofollow">我要注册</a>&nbsp;&nbsp;<a href="" rel="nofollow">找回密码</a>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#header-navbar" aria-expanded="false"><span class="sr-only"></span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                <h1 class="logo hvr-bounce-in"><a href="" title=""><img src="/user/images/logo.png" alt=""></a></h1>
            </div>
            <div class="collapse navbar-collapse" id="header-navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-index active"><a data-cont="异清轩首页" href="index.html">异清轩首页</a></li>
                    <li><a href="category.html">前端技术</a></li>
                    <li><a href="category.html">后端程序</a></li>
                    <li><a href="category.html">管理系统</a></li>
                    <li><a href="category.html">授人以渔</a></li>
                    <li><a href="category.html">程序人生</a></li>
                </ul>
                <form class="navbar-form visible-xs" action="/Search" method="post">
                    <div class="input-group">
                        <input type="text" name="keyword" class="form-control" placeholder="请输入关键字" maxlength="20"
                               autocomplete="off">
                        <span class="input-group-btn">
            <button class="btn btn-default btn-search" name="search" type="submit">搜索</button>
            </span></div>
                </form>
            </div>
        </div>
    </nav>
</header>
<section class="container">
    <!--主体内容-->
    <div class="main">
        <div class="row">
            <div class="col-md-2 col-md-offset-5 head">
                <h3>我的资源</h3>
            </div>
        </div>
        <c:if test="${empty myRes}">  <%--如果“我的资源”列表为空则提示未上传过资源--%>
            <h4>抱歉，您还未上传过任何资源哦！</h4>
        </c:if>
        <c:if test="${!empty myRes}"> <%--如果“我的资源”列表不为空则对其进行遍历显示--%>
            <c:forEach items="${myRes}" var="res" varStatus="itemStr">
                <div class="row">
                    <div class="col-md-9">
                        <c:if test="${itemStr.index%3 == 0}"><%--根据顺序三种颜色循环排列--%>
                        <div class="panel panel-info panel_rlt">
                        </c:if>
                        <c:if test="${itemStr.index%3 == 1}">
                        <div class="panel panel-warning panel_rlt">
                        </c:if>
                        <c:if test="${itemStr.index%3 == 2}">
                        <div class="panel panel-success panel_rlt">
                        </c:if>
                            <div class="panel-heading">
                                <h3 class="panel-title">${res.resName}</h3>
                                <c:if test="${itemStr.index%3 == 0}"><%--根据顺序三种颜色循环排列--%>
                                    <h3 class="panel-title resType"><span
                                            class="label label-info">${res.category.name}</span></h3>
                                </c:if>
                                <c:if test="${itemStr.index%3 == 1}">
                                    <h3 class="panel-title resType"><span
                                            class="label label-warning">${res.category.name}</span></h3>
                                </c:if>
                                <c:if test="${itemStr.index%3 == 2}">
                                    <h3 class="panel-title resType"><span
                                            class="label label-success">${res.category.name}</span></h3>
                                </c:if>
                            </div>
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-left">
                                        <c:choose>
                                            <c:when test="${res.type == 'doc'}">
                                                <img src="/user/images/resType/resType_doc.svg"
                                                     class="media-object" style="width:60px; height:60px">
                                            </c:when>
                                            <c:when test="${res.type == 'docx'}">
                                                <img src="/user/images/resType/resType_docx.svg"
                                                     class="media-object" style="width:60px; height:60px">
                                            </c:when>
                                            <c:when test="${res.type == 'exe'}">
                                                <img src="/user/images/resType/resType_exe.svg"
                                                     class="media-object" style="width:60px; height:60px">
                                            </c:when>
                                            <c:when test="${res.type == 'pdf'}">
                                                <img src="/user/images/resType/resType_pdf.svg"
                                                     class="media-object" style="width:60px; height:60px">
                                            </c:when>
                                            <c:when test="${res.type == 'rar'}">
                                                <img src="/user/images/resType/resType_rar.svg"
                                                     class="media-object" style="width:60px; height:60px">
                                            </c:when>
                                            <c:when test="${res.type == 'txt'}">
                                                <img src="/user/images/resType/resType_txt.svg"
                                                     class="media-object" style="width:60px; height:60px">
                                            </c:when>
                                            <c:when test="${res.type == 'zip'}">
                                                <img src="/user/images/resType/resType_zip.svg"
                                                     class="media-object" style="width:60px; height:60px">
                                            </c:when>
                                            <c:otherwise>
                                                <img src="/user/images/resType/resType_unknown.svg"
                                                     class="media-object" style="width:60px; height:60px">
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="media-body">
                                        <p>${res.dsb}</p>
                                        <div class="resDetail">
                                            <!--  <small >2021-3-31 大小 类型 下载量 所需积分</small>-->
                                            <small>
                                                <ul>
                                                    <li>${res.uploadTime}</li>
                                                    <li>${res.size}MB</li>
                                                    <li>${res.type}</li>
                                                    <li>下载量: ${res.download}</li>
                                                    <li>所需积分: ${res.scores}</li>
                                                </ul>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="text-align: center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-info" id="editMyres">编辑</button>
                            <button type="button" class="btn btn-default" id="dldMyres">下载</button>
                            <button type="button" class="btn btn-warning" id="delMyres" onclick="delMyRes(${res.resourcesId})">删除</button>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </c:if>
    </div>
</section>
<footer class="footer">
    <div class="container">
        <p>&copy; 2016 <a href="">ylsat.com</a> &nbsp; <a href="#" target="_blank" rel="nofollow">豫ICP备20151109-1</a>
            &nbsp; &nbsp; <a href="http://www.mycodes.net/" target="_blank">源码之家</a></p>
    </div>
    <div id="gotop"><a class="gotop"></a></div>
</footer>

<!--微信二维码模态框-->
<div class="modal fade user-select" id="WeChat" tabindex="-1" role="dialog" aria-labelledby="WeChatModalLabel">
    <div class="modal-dialog" role="document" style="margin-top:120px;max-width:280px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="WeChatModalLabel" style="cursor:default;">微信扫一扫</h4>
            </div>
            <div class="modal-body" style="text-align:center"><img src="/user/images/weixin.jpg" alt=""
                                                                   style="cursor:pointer"/></div>
        </div>
    </div>
</div>
<!--该功能正在日以继夜的开发中-->
<div class="modal fade user-select" id="areDeveloping" tabindex="-1" role="dialog"
     aria-labelledby="areDevelopingModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="areDevelopingModalLabel" style="cursor:default;">该功能正在日以继夜的开发中…</h4>
            </div>
            <div class="modal-body"><img src="/user/images/baoman/baoman_01.gif" alt="深思熟虑"/>
                <p style="padding:15px 15px 15px 100px; position:absolute; top:15px; cursor:default;">
                    很抱歉，程序猿正在日以继夜的开发此功能，本程序将会在以后的版本中持续完善！</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">朕已阅</button>
            </div>
        </div>
    </div>
</div>
<!--登录注册模态框-->
<div class="modal fade user-select" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/Admin/Index/login" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="loginModalLabel">登录</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="loginModalUserNmae">用户名</label>
                        <input type="text" class="form-control" id="loginModalUserNmae" placeholder="请输入用户名" autofocus
                               maxlength="15" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="loginModalUserPwd">密码</label>
                        <input type="password" class="form-control" id="loginModalUserPwd" placeholder="请输入密码"
                               maxlength="18" autocomplete="off" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">登录</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="/user/js/bootstrap.min.js"></script>
<script src="/user/js/jquery.ias.js"></script>
<script src="/user/js/scripts.js"></script>
<script type="text/javascript" src="/user/js/myres.js"></script>
</body>
</html>
