<%--
  Created by IntelliJ IDEA.
  User: 11327
  Date: 2021/3/29
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>资源上传</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="apple-touch-icon-precomposed" href="images/icon/icon.png">
    <link rel="shortcut icon" href="images/icon/favicon.ico">
    <link type="text/css" rel="stylesheet" href="css/upload_res.css">
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script src="js/jquery.lazyload.min.js"></script>
    <!--[if gte IE 9]>
    <script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="js/html5shiv.min.js" type="text/javascript"></script>
    <script src="js/respond.min.js" type="text/javascript"></script>
    <script src="js/selectivizr-min.js" type="text/javascript"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script>window.location.href = '/upgrade-browser.html';</script>
    <![endif]-->
</head>
<body class="user-select">
<header class="header">
    <nav class="navbar navbar-default" id="navbar">
        <div class="container">
            <div class="header-topbar hidden-xs link-border">
                <ul class="site-nav topmenu">
                    <li><a href="tags.html">标签云</a></li>
                    <li><a href="readers.html" rel="nofollow">读者墙</a></li>
                    <li><a href="links.html" rel="nofollow">友情链接</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false" rel="nofollow">关注本站 <span
                            class="caret"></span></a>
                        <ul class="dropdown-menu header-topbar-dropdown-menu">
                            <li><a data-toggle="modal" data-target="#WeChat" rel="nofollow"><i class="fa fa-weixin"></i>
                                微信</a></li>
                            <li><a href="#" rel="nofollow"><i class="fa fa-weibo"></i> 微博</a></li>
                            <li><a data-toggle="modal" data-target="#areDeveloping" rel="nofollow"><i
                                    class="fa fa-rss"></i> RSS</a></li>
                        </ul>
                    </li>
                </ul>
                <a data-toggle="modal" data-target="#loginModal" class="login" rel="nofollow">Hi,请登录</a>&nbsp;&nbsp;<a
                    href="javascript:;" class="register" rel="nofollow">我要注册</a>&nbsp;&nbsp;<a href="" rel="nofollow">找回密码</a>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#header-navbar" aria-expanded="false"><span class="sr-only"></span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                <h1 class="logo hvr-bounce-in"><a href="" title=""><img src="images/logo.png" alt=""></a></h1>
            </div>
            <div class="collapse navbar-collapse" id="header-navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-index active"><a data-cont="异清轩首页" href="index.html">异清轩首页</a></li>
                    <li><a href="category.html">前端技术</a></li>
                    <li><a href="category.html">后端程序</a></li>
                    <li><a href="category.html">管理系统</a></li>
                    <li><a href="category.html">授人以渔</a></li>
                    <li><a href="category.html">程序人生</a></li>
                </ul>
                <form class="navbar-form visible-xs" action="/Search" method="post">
                    <div class="input-group">
                        <input type="text" name="keyword" class="form-control" placeholder="请输入关键字" maxlength="20"
                               autocomplete="off">
                        <span class="input-group-btn">
            <button class="btn btn-default btn-search" name="search" type="submit">搜索</button>
            </span></div>
                </form>
            </div>
        </div>
    </nav>
</header>
<section class="container">

    <!--主体内容-->
    <div class="main">
        <h3>上传资源 <small>声明：请确保您上传的内容合法合规，涉及侵权或违规内容将会被移除</small></h3>

        <br><br>
        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" id="uploadForm">
            <div class="form-group">
                <label for="file" class="col-sm-2 control-label">选择文件</label>
                <div class="col-sm-8">
                    <input type="file" id="file" name="file">
                    <span id="helpBlock" class="help-block">选择文件后，文件的大小及类型将在下方显示</span>
                </div>
            </div>

            <div class="form-group">
                <label for="resName" class="col-sm-2 control-label">资源名称</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" placeholder="请输入名称" id="resName" name="resName" required autocomplete="false">
                </div>
            </div>
            <div class="form-group">
                <label for="size" class="col-sm-2 control-label">文件大小</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" readonly placeholder="请选择文件" id="size" name="size">
                </div>
                <label for="size" class="control-label">Mb(上传上限为100Mb)</label>
            </div>
            <div class="form-group">
                <label for="type" class="col-sm-2 control-label">文件类型</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" readonly id="type" placeholder="请选择文件" name="type" >
                </div>
                <div class="col-sm-2">
                    <img src="" alt="" class="img-rounded" id="typeImg">
                </div>
            </div>

            <div class="form-group">
                <label for="dsb" class="col-sm-2 control-label">资源描述</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" placeholder="简要介绍一下你上传的资源" id="dsb" name="dsb"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">所属类别</label>
                <label class="radio-inline">
                    <input type="radio" name="category" id="inlineRadio1" value="1" > java
                </label>
                <label class="radio-inline">
                    <input type="radio" name="category" id="inlineRadio2" value="2"> 运维
                </label>
                <label class="radio-inline">
                    <input type="radio" name="category" id="inlineRadio3" value="5"> 微服务
                </label>
                <label class="radio-inline">
                    <input type="radio" name="category" id="inlineRadio4" value="6"> 数据库
                </label>
            </div>

            <div class="form-group">
                <label for="scores" class="col-sm-2 control-label">所需积分</label>
                <div class="col-sm-2">
                    <select class="form-control" id="scores" name="scores">
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-default" onclick="mySubmit()">提交</button>
                </div>
            </div>
        </form>

        <%--上传成功和失败的两个框，根据后台返回的结果进行动态显示， 默认隐藏--%>
        <div id="updSuccess" class="updRes" hidden="hidden">
            <p>
                上传成功！
            </p>
            <p>
                <a class="btn btn-default" href="#" role="button">查看我的上传</a>
                <a class="btn btn-default" href="upload_res.jsp" role="button">继续上传</a>
            </p>
        </div>
        <div id="updFail" class="updRes" hidden="hidden">
            <p>
                上传失败...
            </p>
            <p>
                <a class="btn btn-default" href="#" role="button">回到首页</a>
                <a class="btn btn-default" href="upload_res.jsp" role="button">重新上传</a>
            </p>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="container">
        <p>&copy; 2016 <a href="">ylsat.com</a> &nbsp; <a href="#" target="_blank" rel="nofollow">豫ICP备20151109-1</a>
            &nbsp; &nbsp; <a href="http://www.mycodes.net/" target="_blank">源码之家</a></p>
    </div>
    <div id="gotop"><a class="gotop"></a></div>
</footer>

<%--上传结果模态框--%>
<div class="modal fade" tabindex="-1" role="dialog" id="updSuccessModel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">上传成功！</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">查看我的上传</button>
                <button type="button" class="btn btn-primary">继续上传</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="updFailModel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">上传失败！</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">回到首页</button>
                <button type="button" class="btn btn-primary">重新上传</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--微信二维码模态框-->
<div class="modal fade user-select" id="WeChat" tabindex="-1" role="dialog" aria-labelledby="WeChatModalLabel">
    <div class="modal-dialog" role="document" style="margin-top:120px;max-width:280px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="WeChatModalLabel" style="cursor:default;">微信扫一扫</h4>
            </div>
            <div class="modal-body" style="text-align:center"><img src="images/weixin.jpg" alt=""
                                                                   style="cursor:pointer"/></div>
        </div>
    </div>
</div>
<!--该功能正在日以继夜的开发中-->
<div class="modal fade user-select" id="areDeveloping" tabindex="-1" role="dialog"
     aria-labelledby="areDevelopingModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="areDevelopingModalLabel" style="cursor:default;">该功能正在日以继夜的开发中…</h4>
            </div>
            <div class="modal-body"><img src="images/baoman/baoman_01.gif" alt="深思熟虑"/>
                <p style="padding:15px 15px 15px 100px; position:absolute; top:15px; cursor:default;">
                    很抱歉，程序猿正在日以继夜的开发此功能，本程序将会在以后的版本中持续完善！</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">朕已阅</button>
            </div>
        </div>
    </div>
</div>
<!--登录注册模态框-->
<div class="modal fade user-select" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/Admin/Index/login" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="loginModalLabel">登录</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="loginModalUserNmae">用户名</label>
                        <input type="text" class="form-control" id="loginModalUserNmae" placeholder="请输入用户名" autofocus
                               maxlength="15" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="loginModalUserPwd">密码</label>
                        <input type="password" class="form-control" id="loginModalUserPwd" placeholder="请输入密码"
                               maxlength="18" autocomplete="off" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">登录</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.ias.js"></script>
<script src="js/scripts.js"></script>
<script type="text/javascript">
    // var file = $("input[type='file']")[0];
    var file = $("#file")
    //点击时触发
    file.on("change", function () {
        var cur = this;

        if (cur.files[0] != null){
            //将读取到的文件大小写入文件大小输入框
            var sizeOrg = cur.files[0].size;
            //保留小数点后两位
            var sizeMb = Math.round(sizeOrg / 1024 / 1024 * 100) / 100;
            $("#size").val(sizeMb);
            console.log("size(字节):" + sizeOrg);
            console.log("sizeMb(Mb):" + sizeMb);

            //将读取到的文件名剥离出类型  写入文件类型输入框
            var file = $("#file").val();
            var fileType = getFileType(file);
            $("#type").val(fileType);

            //获取读取到的文件名 写入 资源名称框
            var fileName = getFileName(file);
            $("#resName").val(fileName);

            //同时根据文件类型 显示该类型对应的图片   rar/zip/txt/pdf/doc/docx /exe ?
            var typeImg = $("#typeImg");
            switch (fileType) {
                case 'rar':
                    typeImg.prop("src", "images/resType/resType_rar.svg");
                    break;
                case 'zip':
                    typeImg.prop("src", "images/resType/resType_zip.svg");
                    break;
                case 'txt':
                    typeImg.prop("src", "images/resType/resType_txt.svg");
                    break;
                case 'pdf':
                    typeImg.prop("src", "images/resType/resType_pdf.svg");
                    break;
                case 'doc':
                    typeImg.prop("src", "images/resType/resType_doc.svg");
                    break;
                case 'docx':
                    typeImg.prop("src", "images/resType/resType_docx.svg");
                    break;
                case 'exe':
                    typeImg.prop("src", "images/resType/resType_exe.svg");
                    break;
                default:
                    typeImg.prop("src", "images/resType/resType_unknown.svg");
            }
        }else {
            console.log("为null");
        }
    });

    /*获取文件 类型  .之后的， 如txt  rar等*/
    function getFileType(o) {
        // var pos=o.lastIndexOf('\\');  //转义一下
        // var fileName = o.substring(pos+1);
        var pos=o.lastIndexOf('.');  //转义一下
        var fileType = o.substring(pos+1);
        return fileType;
    }

    function getFileName(o) {
        var pos=o.lastIndexOf('\\');  //转义一下
        var fileName = o.substring(pos+1);
        return fileName;
    }

    function mySubmit() {
        // // 获取资源名称 resName
        // var resName = $("#resName").val();
        // // 获取文件上传 file
        // var file = $("#file");
        // //获取文件大小 size
        // var size = $("#size").val();
        // //获取文件类型 type
        // var type = $("#type").val();
        // //获取文件所属类别 category (单选框 name)
        // var categorys = document.getElementsByName("category");
        // var category;c
        // for (var i = 0; i < categorys.length; i++)
        // {
        //     console.log("进循环了");
        //     if (categorys[i].checked){
        //         category = categorys[i].value;
        //     }
        // }
        // //获取文件下载所需积分 scores
        // var scores = $("#scores option:selected").val();
        //
        // console.log("resName:" + resName);
        // console.log("file:" + file);
        // console.log("size:" + size);
        // console.log("type:" + type);
        // console.log("category:" + category);
        // console.log("scores:" + scores);

        var formData = new FormData($('#uploadForm')[0]);

        // //通过ajax提交表单
        $.ajax({
            url: 'http://localhost:8080/res/uploadRes', //要访问的地址  这里使用绝对定位  / 表示到8080/
            data:formData, //将参数传到后台的login方法中      注意'' 之间的字符串里不要有空格，不然会识别不出类型
            type:'post', //提交方式
            dataType:'text', //如果返回的是对象或者集合，那么数据类型是json
            cache: false, //设置是否第二次是否从缓存中读取
            processData: false,//将数据类型序列换成application/x-www-form-urlencoded ，fasle表示关闭  禁止设置请求类型
            contentType: false,//默认值为application/x-www-form-urlencoded，false表示关闭  禁止jquery对DAta数据的处理,默认会处理 禁止的原因是,FormData已经帮我们做了处理
            success: function (result) {  //result就是后台的返回值
                if (result == 1){  //上传成功
                    // $("#updSuccess").removeAttr("hidden");
                    $("#updSuccessModel").modal();

                }else{ //上传失败
                    // $("#updFail").removeAttr("hidden");
                    $("#updFailModel").modal();
                }
            }
        });
    }
</script>
</body>
</html>
