package cn.ncucode.blog.bean;

/**
 * 前端与用户交互的提示信息
 * 
 * 0 : 一切正常
 * 
 * -2 : "邮箱已存在"
 */
public class Info {
	int code;
	String info;

	static {
		new Info(0, "正常");
		new Info(-1, "邮箱未被注册");
		new Info(-2, "邮箱已存在");
		new Info(-3, "验证码错误");
		new Info(-4, "未登录状态");
		new Info(-5, "用户名或密码错误");
		new Info(-6, "用户名已存在");
		new Info(-7, "密码错误");
		new Info(-8, "参数错误");
		new Info(-9, "权限不足");
		new Info(-10, "服务器异常");
	}

	public Info(int code, String info) {
		this.code = code;
		this.info = info;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
}
