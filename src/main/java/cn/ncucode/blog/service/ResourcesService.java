package cn.ncucode.blog.service;

import cn.ncucode.blog.model.Resources;

import java.util.List;

public interface ResourcesService {
    //新增一个资源
    int addResources(Resources resources);

    //获取所有资源
    List<Resources> getAllResources();

    //获取用户自己的上传资源
    List<Resources> getResourcesByUserId(long userId);
}
