package cn.ncucode.blog.service.impl;

import cn.ncucode.blog.dao.ResourcesDao;
import cn.ncucode.blog.model.Resources;
import cn.ncucode.blog.service.ResourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ResourcesServiceImpl implements ResourcesService {

    @Autowired  //默认先根据类型匹配
    @Qualifier("resourcesDao")   //指定id
    private ResourcesDao resourcesDao;

    @Override  //这个注解说明下面的方法是重写父类的方法
    public int addResources(Resources resources) {
        return resourcesDao.addResources(resources);
    }

    @Override
    public List<Resources> getAllResources() {
        return resourcesDao.getAllResources();
    }

    @Override
    public List<Resources> getResourcesByUserId(long userId) {
        return resourcesDao.getResourcesByUserId(userId);
    }
}
