package cn.ncucode.blog.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.ncucode.blog.dao.UserDao;
import cn.ncucode.blog.model.User;
import cn.ncucode.blog.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	UserDao userDao;

	@Override
	public User getUserById(long id) {
		User user = new User();
		user.setUserid(id);
		return userDao.getUserPrivate(user);
	}

	/**
	 * @return 如果邮箱已注册，返回true
	 */
	@Override
	public boolean emailExist(String email) {
		if (userDao.getUserByEmail(email) != null) {
			return true;
		}
		return false;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public void registUser(User user) {
		user.setRegisttime(new Date());
		userDao.insert(user);
	}

	@Override
	public boolean usernameExist(String username) {
		User search = new User();
		search.setUsername(username);
		if (userDao.getUser(search) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void resetPass(String email, String password) {
		User user = new User();
		user.setEmail(email);
		user = userDao.getUser(user);
		User saveUser = new User();
		saveUser.setUserid(user.getUserid());
		saveUser.setPassword(password);
		userDao.updateUserSelective(saveUser);
	}

	@Override
	public User login(User user) {
		User check = new User();
		check.setUsername(user.getUsername());
		check.setPassword(user.getPassword());
		System.out.println(check);
		user = userDao.getUserPrivate(check);
		if (user == null) {
			return null;
		}
		System.out.println(user);
		System.out.println(user.getScores());
		if (user.getLastlogin() == null) {
			// 首次登陆
			// 设置积分
			user.setScores(user.getScores() + 10);
			user.setSign(1);

			user.setLastlogin(new Date());
			userDao.updateUserSelective(user);
			user.setLastlogin(null);
			return user;
		} else {
			// 非首次登陆
			long offset = 8 * 1000 * 60 * 60;
			long date1 = (new Date().getTime() + offset) / 1000 / 60 / 60 / 24;
			long date2 = (user.getLastlogin().getTime() + offset) / 1000 / 60 / 60 / 24;
			if (date1 == date2) {
				// 同一天登陆
			} else {
				if (date1 - date2 == 1) {
					// 相隔一天
					user.setSign(user.getSign() + 1);
				} else {
					// 相隔多天
					// 设置连续签到为一天
					user.setSign(1);
				}
				// 添加积分
				user.setScores(user.getScores() + user.getSign());
			}
		}
		Date lastDate = user.getLastlogin();
		user.setLastlogin(new Date());
		userDao.updateUserSelective(user);
		user.setLastlogin(lastDate);
		return user;
	}

	@Override
	public User getUserByIdPass(long id, String password) {
		User user = new User();
		user.setUserid(id);
		user.setPassword(password);
		return userDao.getUser(user);
	}

	@Override
	public void updateInfo(long id, String nickname, String password) {
		User user = new User();
		user.setUserid(id);
		user.setPassword(password);
		user.setNickname(nickname);
		userDao.updateUserSelective(user);
	}

}
