package cn.ncucode.blog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.ncucode.blog.dao.FollowDao;
import cn.ncucode.blog.model.Follow;
import cn.ncucode.blog.service.FollowService;

@Service
public class FollowServiceImpl implements FollowService {

	FollowDao followDao;

	@Override
	public void add(long user1, long user2) {
		Follow follow = new Follow(user1, user2);
		try {
			followDao.insert(follow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cancle(long user1, long user2) {
		Follow follow = new Follow(user1, user2);
		try {
			followDao.deleteByPrimaryKey(follow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FollowDao getFollowDao() {
		return followDao;
	}

	@Autowired
	public void setFollowDao(FollowDao followDao) {
		this.followDao = followDao;
	}

	@Override
	public long getFansnumById(long userId) {
		return followDao.getFansnumByUser(userId);
	}

	@Override
	public boolean getStatus(long user1, long user2) {
		if (followDao.getByPrimaryKey(new Follow(user1, user2)) == null) {
			return false;
		} else {
			return true;
		}
	}

}
