package cn.ncucode.blog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.ncucode.blog.dao.CategoryDao;
import cn.ncucode.blog.model.Category;
import cn.ncucode.blog.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	CategoryDao categoryDao;

	@Override
	public List<Category> getAll() {
		return categoryDao.getAll();
	}

	public CategoryDao getCategoryDao() {
		return categoryDao;
	}

	@Autowired
	public void setCategoryDao(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
	}

	@Override
	public void add(String name) {
		Category category = new Category();
		category.setName(name);
		System.out.println("CategoryServiceImpl.add()");
		System.out.println(categoryDao.insert(category));
		System.out.println(category.getCategoryid());
		System.out.println("CategoryServiceImpl.add()");
	}

}
