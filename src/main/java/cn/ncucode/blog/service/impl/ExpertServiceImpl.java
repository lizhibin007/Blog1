package cn.ncucode.blog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.ncucode.blog.dao.ExpertDao;
import cn.ncucode.blog.model.Expert;
import cn.ncucode.blog.service.ExpertService;

@Service
public class ExpertServiceImpl implements ExpertService {

	ExpertDao expertDao;

	@Override
	public void apply(long userId, int category) {
		Expert expert = new Expert(category, userId, (byte) 0);
		expertDao.insert(expert);
	}

	/**
	 * @return 0: 申请中 1：已成为专家 2: 未申请
	 */
	@Override
	public int getStatus(long userId, int category) {
		Expert expert = new Expert(category, userId);
		expert = expertDao.getExpertSelective(expert);
		if (expert == null) {
			return 2;
		}
		return expert.getHandle();
	}

	public ExpertDao getExpertDao() {
		return expertDao;
	}

	@Autowired
	public void setExpertDao(ExpertDao expertDao) {
		this.expertDao = expertDao;
	}

	@Override
	public List<Expert> getAllUnhandle() {
		Expert expert = new Expert(null, null, (byte) 0);
		return expertDao.getExpertsSelective(expert);
	}

	@Override
	public List<Expert> getAll() {
		return expertDao.getAll();
	}

	@Override
	public void reject(long userId, int category) {
		Expert expert = new Expert(category, userId);
		try {
			expertDao.deleteByPrimaryKey(expert);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void agree(long userId, int category) {
		Expert expert = new Expert(category, userId);
		expert = expertDao.getExpertSelective(expert);
		try {
			if (expert.getHandle() == 0) {
				expert.setHandle((byte) 1);
			}
			expertDao.updateByPrimaryKey(expert);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
