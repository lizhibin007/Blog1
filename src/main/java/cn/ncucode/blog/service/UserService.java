package cn.ncucode.blog.service;

import cn.ncucode.blog.model.User;

import java.util.List;

public interface UserService {

	public User getUserById(long id);

	public User getUserByIdPass(long id, String password);

	public boolean emailExist(String email);

	public boolean usernameExist(String username);

	public void registUser(User user);

	public void resetPass(String email, String password);

	public User login(User user);

	public void updateInfo(long id, String nickName, String password);
}
