package cn.ncucode.blog.service;

import java.util.List;

import cn.ncucode.blog.model.Expert;

public interface ExpertService {

	public int getStatus(long userId, int category);

	public void apply(long userId, int category);

	public List<Expert> getAllUnhandle();

	public List<Expert> getAll();

	public void reject(long userId, int category);

	public void agree(long userId, int category);
}
