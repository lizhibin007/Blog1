package cn.ncucode.blog.service;

import javax.swing.text.StyledEditorKit.BoldAction;

/**
 * FollowService
 */
public interface FollowService {

	public void add(long user1, long user2);

	public void cancle(long user1, long user2);

	public long getFansnumById(long userId);

	public boolean getStatus(long user1, long user2);
}