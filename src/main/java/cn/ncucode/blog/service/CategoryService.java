package cn.ncucode.blog.service;

import java.util.List;

import cn.ncucode.blog.model.Category;

public interface CategoryService {
	public List<Category> getAll();

	public void add(String name);

}
