package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Blog;

public interface BlogDao {
    int deleteByPrimaryKey(Long blogid);

    int insert(Blog record);

    int insertSelective(Blog record);

    Blog selectByPrimaryKey(Long blogid);

    int updateByPrimaryKeySelective(Blog record);

    int updateByPrimaryKey(Blog record);
}