package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Mycolumn;

public interface MycolumnDao {
    int deleteByPrimaryKey(Long columnid);

    int insert(Mycolumn record);

    int insertSelective(Mycolumn record);

    Mycolumn selectByPrimaryKey(Long columnid);

    int updateByPrimaryKeySelective(Mycolumn record);

    int updateByPrimaryKey(Mycolumn record);
}