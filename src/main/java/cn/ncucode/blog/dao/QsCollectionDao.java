package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.QsCollectionKey;

public interface QsCollectionDao {
    int deleteByPrimaryKey(QsCollectionKey key);

    int insert(QsCollectionKey record);

    int insertSelective(QsCollectionKey record);
}