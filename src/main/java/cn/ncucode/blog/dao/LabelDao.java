package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Label;

public interface LabelDao {
    int deleteByPrimaryKey(Integer lableid);

    int insert(Label record);

    int insertSelective(Label record);

    Label selectByPrimaryKey(Integer lableid);

    int updateByPrimaryKeySelective(Label record);

    int updateByPrimaryKey(Label record);
}