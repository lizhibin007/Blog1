package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Report;

public interface ReportDao {
    int deleteByPrimaryKey(Long reportid);

    int insert(Report record);

    int insertSelective(Report record);

    Report selectByPrimaryKey(Long reportid);

    int updateByPrimaryKeySelective(Report record);

    int updateByPrimaryKey(Report record);
}