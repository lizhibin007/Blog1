package cn.ncucode.blog.dao;

import java.util.List;

import cn.ncucode.blog.model.Category;

public interface CategoryDao {

	public List<Category> getAll();

	int deleteByPrimaryKey(Integer categoryid);

	public int insert(Category record);

	int insertSelective(Category record);

	Category selectByPrimaryKey(Integer categoryid);

	int updateByPrimaryKeySelective(Category record);

	int updateByPrimaryKey(Category record);
}