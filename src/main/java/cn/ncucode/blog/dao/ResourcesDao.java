package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Resources;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourcesDao {
    //增加一项资源
    int addResources(Resources resources);

    //查询所有资源
    List<Resources> getAllResources();

    //根据用户id查询该用户所上传的资源
    List<Resources> getResourcesByUserId(@Param("uid") long userId);
}

