package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.QsSubscribeKey;

public interface QsSubscribeDao {
    int deleteByPrimaryKey(QsSubscribeKey key);

    int insert(QsSubscribeKey record);

    int insertSelective(QsSubscribeKey record);
}