package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.ReportType;

public interface ReportTypeDao {
    int deleteByPrimaryKey(Integer typeid);

    int insert(ReportType record);

    int insertSelective(ReportType record);

    ReportType selectByPrimaryKey(Integer typeid);

    int updateByPrimaryKeySelective(ReportType record);

    int updateByPrimaryKey(ReportType record);
}