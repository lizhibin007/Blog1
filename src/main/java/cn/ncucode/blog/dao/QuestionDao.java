package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Question;

public interface QuestionDao {
    int deleteByPrimaryKey(Long questionid);

    int insert(Question record);

    int insertSelective(Question record);

    Question selectByPrimaryKey(Long questionid);

    int updateByPrimaryKeySelective(Question record);

    int updateByPrimaryKey(Question record);
}