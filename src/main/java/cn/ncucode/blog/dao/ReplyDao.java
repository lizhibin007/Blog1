package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Reply;

public interface ReplyDao {
    int deleteByPrimaryKey(Long replyid);

    int insert(Reply record);

    int insertSelective(Reply record);

    Reply selectByPrimaryKey(Long replyid);

    int updateByPrimaryKeySelective(Reply record);

    int updateByPrimaryKey(Reply record);
}