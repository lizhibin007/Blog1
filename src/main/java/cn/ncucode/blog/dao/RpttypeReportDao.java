package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.RpttypeReportKey;

public interface RpttypeReportDao {
    int deleteByPrimaryKey(RpttypeReportKey key);

    int insert(RpttypeReportKey record);

    int insertSelective(RpttypeReportKey record);
}