package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.BlogCollectionKey;

public interface BlogCollectionDao {
    int deleteByPrimaryKey(BlogCollectionKey key);

    int insert(BlogCollectionKey record);

    int insertSelective(BlogCollectionKey record);
}