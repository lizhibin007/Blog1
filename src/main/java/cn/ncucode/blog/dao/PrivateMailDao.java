package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.PrivateMail;

public interface PrivateMailDao {
    int deleteByPrimaryKey(Long mailid);

    int insert(PrivateMail record);

    int insertSelective(PrivateMail record);

    PrivateMail selectByPrimaryKey(Long mailid);

    int updateByPrimaryKeySelective(PrivateMail record);

    int updateByPrimaryKey(PrivateMail record);
}