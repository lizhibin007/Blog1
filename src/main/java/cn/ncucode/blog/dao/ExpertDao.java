package cn.ncucode.blog.dao;

import java.util.List;

// import java.util.List;

import cn.ncucode.blog.model.Expert;

public interface ExpertDao {

	public Expert getExpertSelective(Expert expert);

	public int insert(Expert record);

	public List<Expert> getExpertsSelective(Expert expert);

	public int deleteByPrimaryKey(Expert key);

	public List<Expert> getAll();

	public int updateByPrimaryKey(Expert record);

	// public List<Expert> getExpertsSelective(Expert expert);

	// int insertSelective(Expert record);

	// Expert selectByPrimaryKey(Expert key);

	// int updateByPrimaryKeySelective(Expert record);

}