package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.LabelBlogKey;

public interface LabelBlogDao {
    int deleteByPrimaryKey(LabelBlogKey key);

    int insert(LabelBlogKey record);

    int insertSelective(LabelBlogKey record);
}