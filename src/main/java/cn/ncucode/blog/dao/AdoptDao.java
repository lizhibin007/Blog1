package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.AdoptKey;

public interface AdoptDao {
    int deleteByPrimaryKey(AdoptKey key);

    int insert(AdoptKey record);

    int insertSelective(AdoptKey record);
}