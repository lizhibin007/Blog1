package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Notice;

public interface NoticeDao {
    int deleteByPrimaryKey(Long noticeid);

    int insert(Notice record);

    int insertSelective(Notice record);

    Notice selectByPrimaryKey(Long noticeid);

    int updateByPrimaryKeySelective(Notice record);

    int updateByPrimaryKey(Notice record);
}