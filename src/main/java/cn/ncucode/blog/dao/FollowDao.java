package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.Follow;

public interface FollowDao {
	int deleteByPrimaryKey(Follow key);

	int insert(Follow record);

	public long getFansnumByUser(long userId);

	public Follow getByPrimaryKey(Follow follow);
}