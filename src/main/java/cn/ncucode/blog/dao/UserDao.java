package cn.ncucode.blog.dao;

import java.util.List;

import cn.ncucode.blog.model.User;


public interface UserDao {

	/**
	 * 
	 * @param email
	 * @return null: 找不到匹配的用户
	 */
	User getUserByEmail(String email);

	/**
	 * 添加一个新用户
	 * 
	 * @param user 必需提供以下几个属性(userName, nickName, registTime, email, password)
	 */
	void insert(User user);

	/**
	 * 通过多条件查找，如果为null表示忽略条件
	 * 
	 * @param user 查找条件
	 * @return 返回一个符合条件对象
	 */
	User getUser(User user);

	/**
	 * 通过多条件查找，如果为null表示忽略条件
	 * 
	 * @param user 查找条件
	 * @return 返回多个符合条件对象
	 */
	List<User> getUsers(User user);

	/**
	 * 通过多条件查找，如果为null表示忽略条件
	 * 
	 * @param user 查找条件
	 * @return 返回一个符合条件对象
	 */
	User getUserPrivate(User user);

	/**
	 * 选择性更新User数据
	 * 
	 * @param user
	 * @return
	 */
	int updateUserSelective(User user);

	User getUserByPassAndEmail(User user);

	User getUserByUsername(User user);

	int deleteByPrimaryKey(Long userid);

	int insertSelective(User record);

	User selectByPrimaryKey(Long userid);

	int updateByPrimaryKey(User record);
}
