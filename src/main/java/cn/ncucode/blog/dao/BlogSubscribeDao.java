package cn.ncucode.blog.dao;

import cn.ncucode.blog.model.BlogSubscribeKey;

public interface BlogSubscribeDao {
    int deleteByPrimaryKey(BlogSubscribeKey key);

    int insert(BlogSubscribeKey record);

    int insertSelective(BlogSubscribeKey record);
}