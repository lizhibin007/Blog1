package cn.ncucode.blog.test;

import java.util.Date;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.ncucode.blog.dao.UserDao;
import cn.ncucode.blog.model.User;

public class XuApp {
	public static void main(String[] args) {
		new XuApp().test1();
	}

	public void test1() {

		ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(
				"applicationContext.xml");

		UserDao userDao = classPathXmlApplicationContext.getBean(cn.ncucode.blog.dao.UserDao.class);

		User user = userDao.getUserByEmail("ad");

		System.out.println();
		System.out.println(user);
		System.out.println();

		userDao.insert(new User("xh2958", "辉太郎", new Date(), "2958915385@qq.com", "123456"));

		System.out.println("hello world");

		classPathXmlApplicationContext.close();
	}

}
