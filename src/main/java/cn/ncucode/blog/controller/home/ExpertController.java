package cn.ncucode.blog.controller.home;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ncucode.blog.bean.Info;
import cn.ncucode.blog.model.User;
import cn.ncucode.blog.service.ExpertService;

@Controller
@RequestMapping(value = "/expert")
public class ExpertController {

	ExpertService expertService;

	@RequestMapping(value = "/getStatus")
	@ResponseBody
	public int getStatus(Integer category, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user == null) {
			return 2;
		}
		if (category == null) {
			return 2;
		}
		return expertService.getStatus(user.getUserid(), category);
	}

	@RequestMapping(value = "/apply")
	@ResponseBody
	public Info apply(Integer category, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user == null) {
			return new Info(-4, "未登录状态");
		}
		if (expertService.getStatus(user.getUserid(), category) != 2) {
			return new Info(0, "重复申请");
		}
		try {
			expertService.apply(user.getUserid(), category);
		} catch (Exception e) {
			e.printStackTrace();
			return new Info(-8, "参数错误");
		}
		return new Info(0, "申请成功");
	}

	public ExpertService getExpertService() {
		return expertService;
	}

	@Autowired
	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}

}
