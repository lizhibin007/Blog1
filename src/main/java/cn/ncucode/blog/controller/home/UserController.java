package cn.ncucode.blog.controller.home;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ncucode.blog.bean.Info;
import cn.ncucode.blog.controller.EmailController;
import cn.ncucode.blog.model.User;
import cn.ncucode.blog.service.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	UserService userService;

	@RequestMapping(value = "/update")
	@ResponseBody
	public Info update(User user, String oldpass, HttpSession session) {
		System.out.println(user);
		System.out.println(oldpass);
		if (oldpass == null) {
			return new Info(-7, "密码错误");
		}
		if (user.getPassword() == null || user.getPassword().equals("")) {
			return new Info(-11, "密码不能为空");
		}
		User current = (User) session.getAttribute("user");
		// 密码验证
		User check = userService.getUserByIdPass(current.getUserid(), oldpass);
		if (check == null) {
			return new Info(-7, "密码错误");
		}
		try {
			userService.updateInfo(current.getUserid(), user.getNickname(), user.getPassword());
		} catch (Exception e) {
			return new Info(-10, "服务器异常");
		}
		;
		session.setAttribute("user", userService.getUserById(current.getUserid()));
		return new Info(0, "修改成功");
	}

	@RequestMapping(value = "/logout")
	public String logout(HttpSession session) {
		session.setAttribute("user", null);
		System.out.println("UserController.logout()");
		System.out.println(session.getAttribute("user"));
		return "redirect:/user/index";
	}

	@RequestMapping(value = "/center")
	public String center(Model model, HttpSession session) {
		model.addAttribute("user", session.getAttribute("user"));
		return "admin/main";
	}

	@RequestMapping(value = "/login")
	@ResponseBody
	public Info login(User user, HttpSession session) {
		System.out.println(user);
		if (user.getUsername() == null || user.getPassword() == null) {
			return new Info(-5, "用户名或密码错误");
		}
		user = userService.login(user);
		if (user != null) {
			session.setAttribute("user", user);
			return new Info(0, "登录成功");
		} else {
			return new Info(-5, "用户名或密码错误");
		}
	}

	@RequestMapping(value = "/testEmailAvailable")
	@ResponseBody
	public Info testEmailAvailable(String email) {
		if (userService.emailExist(email)) {
			return new Info(-2, "邮箱已存在");
		}
		return new Info(0, "正常");
	}

	@RequestMapping(value = "/emailRegisted")
	@ResponseBody
	public Info emailRegisted(String email) {
		if (userService.emailExist(email)) {
			return new Info(0, "正常");
		} else {
			return new Info(-1, "邮箱未被注册");
		}
	}

	@RequestMapping(value = "/index")
	public String index() {
		return "user/index";
	}

	@RequestMapping(value = "/add")
	@ResponseBody
	public Info add(User user, String code, HttpSession session) {
		System.out.println(session);
		if (user.getUsername() == null) {
			return new Info(-8, "用户名为空");
		} else {
			Info info = new EmailController().testEmailCode(user.getEmail(), code, session);
			if (info.getCode() != 0) {
				return new Info(-8, "验证码错误");
			} else if (userService.usernameExist(user.getUsername())) {
				return new Info(-8, "用户名已存在");
			} else if (userService.emailExist(user.getEmail())) {
				return new Info(-8, "邮箱已存在");
			} else {
				userService.registUser(user);
				return new Info(0, "注册成功");
			}
		}
	}

	@RequestMapping(value = "/regist")
	public String regist() {
		return "user/regist";
	}

	@RequestMapping(value = "/findPass")
	public String findPass() {
		// 跳转到找回密码界面
		return "user/resetPass";
	}

	@RequestMapping(value = "/resetPass")
	@ResponseBody
	public Info resetPass(User user, String code, HttpSession session) {
		System.out.println(user);
		if (user.getEmail() == null) {
			return new Info(-7, "参数错误");
		} else {
			Info info = new EmailController().testEmailCode(user.getEmail(), code, session);
			if (info.getCode() != 0) {
				return new Info(-7, "验证码错误");
			} else if (userService.emailExist(user.getEmail())) {
				userService.resetPass(user.getEmail(), user.getPassword());
				return new Info(0, "修改成功");
			} else {
				return new Info(-7, "参数错误");
			}
		}
	}

	@RequestMapping(value = "getCurrentUser")
	@ResponseBody
	public User getCurrentUser(HttpSession session) {
		return (User) session.getAttribute("user");
	}

	@RequestMapping(value = "/testUsernameAvailable")
	@ResponseBody
	public Info testUsernameAvailable(String username) {
		if (userService.usernameExist(username)) {
			return new Info(-6, "用户名已存在");
		}
		return new Info(0, "正常");
	}

	public UserService getUserService() {
		return userService;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
