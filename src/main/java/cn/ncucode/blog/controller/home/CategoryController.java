package cn.ncucode.blog.controller.home;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ncucode.blog.bean.Info;
import cn.ncucode.blog.model.Category;
import cn.ncucode.blog.service.CategoryService;

@Controller
@RequestMapping(value = "/category")
public class CategoryController {

	CategoryService categoryService;

	@RequestMapping(value = "getAll")
	@ResponseBody
	public List<Category> getAll() {
		return categoryService.getAll();
	}

	@RequestMapping(value = "add")
	@ResponseBody
	public Info add(String name) {
		categoryService.add(name);
		return new Info(0, "添加成功");
	}

	public CategoryService getCategoryService() {
		return categoryService;
	}

	@Autowired
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

}
