package cn.ncucode.blog.controller.home;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ncucode.blog.bean.Info;
import cn.ncucode.blog.model.User;
import cn.ncucode.blog.service.FollowService;

@Controller
@RequestMapping(value = "/follow")
public class FollowController {

	FollowService followService;

	@RequestMapping(value = "/getStatus")
	@ResponseBody
	public boolean getStatus(Long userId, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user == null) {
			return false;
		}
		return followService.getStatus(user.getUserid(), userId);
	}

	@RequestMapping(value = "/add")
	@ResponseBody
	public Info add(Long userId, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user == null) {
			return new Info(-4, "未登录状态");
		}
		long user1 = user.getUserid();
		long user2 = userId;
		try {
			followService.add(user1, user2);
		} catch (Exception e) {
			return new Info(-8, "参数错误");
		}
		return new Info(0, "关注成功");
	}

	@RequestMapping(value = "/cancel")
	@ResponseBody
	public Info cancel(Long userId, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user == null) {
			return new Info(-4, "未登录状态");
		}
		long user1 = user.getUserid();
		long user2 = userId;
		followService.cancle(user1, user2);
		return new Info(0, "已取消");
	}

	@RequestMapping(value = "/getFansnum")
	@ResponseBody
	public Long getFansnum(Long userId) {
		if (userId == null) {
			return 0L;
		}
		return followService.getFansnumById(userId);
	}

	public FollowService getFollowService() {
		return followService;
	}

	@Autowired
	public void setFollowService(FollowService followService) {
		this.followService = followService;
	}

}
