package cn.ncucode.blog.controller.admin;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ncucode.blog.bean.Info;
import cn.ncucode.blog.model.Expert;
import cn.ncucode.blog.model.User;
import cn.ncucode.blog.service.ExpertService;

@Controller
@RequestMapping(value = "/admin/expert")
public class AdminExpertController {

	ExpertService expertService;

	@RequestMapping(value = "/getAllUnhandle")
	@ResponseBody
	public List<Expert> getAllUnhandle(HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user != null && user.getUsername().equals("admin")) {
			return expertService.getAllUnhandle();
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/getAll")
	@ResponseBody
	public List<Expert> getAll(HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user != null && user.getUsername().equals("admin")) {
			return expertService.getAll();
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/agree")
	@ResponseBody
	public Info agree(Long userId, Integer categoryId, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (user != null && user.getUsername().equals("admin")) {
			expertService.agree(userId, categoryId);
			return new Info(0, "已同意");
		} else {
			return new Info(-9, "权限不足");
		}
	}

	@RequestMapping(value = "/reject")
	@ResponseBody
	public Info reject(Long userId, Integer categoryId, HttpSession session) {
		User user = (User) session.getAttribute("user");

		if (user != null && user.getUsername().equals("admin")) {
			expertService.reject(userId, categoryId);
			return new Info(0, "已拒绝");
		} else {
			return new Info(-9, "权限不足");
		}
	}

	public ExpertService getExpertService() {
		return expertService;
	}

	@Autowired
	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}
}
