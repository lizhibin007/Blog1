package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * blog_collection
 * @author 
 */
@Data
public class BlogCollectionKey implements Serializable {
    /**
     * 博客编号
     */
    private Long blogid;

    /**
     * 用户id
     */
    private Long userid;

    private static final long serialVersionUID = 1L;
}