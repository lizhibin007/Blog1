package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * follow
 * 
 * @author
 */
@Data
public class Follow implements Serializable {
	private Long user1;

	private Long user2;

	private static final long serialVersionUID = 1L;

	public Follow(Long user1, Long user2) {
		this.user1 = user1;
		this.user2 = user2;
	}
}