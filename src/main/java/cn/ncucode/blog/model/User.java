package cn.ncucode.blog.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * user
 * 
 * @author
 */
@Data
public class User implements Serializable {
	private Long userid;

	private String username;

	private String nickname;

	private Date registtime;

	private String email;

	private Integer scores;

	private String password;

	private Integer ban;

	private Integer sign;

	private Date lastlogin;

	private static final long serialVersionUID = 1L;

	public User() {

	}

	public User(Long userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "userid = " + userid + ", username = " + username + ", nickname = " + nickname + ", registtime = "
				+ registtime + ", email = " + email + ", scores = " + scores + ", password = " + password + ", ban = "
				+ ban + ", sign = " + sign + ", lastlogin = " + lastlogin;
	}

	public User(String username, String nickname, Date registtime, String email, String password) {
		this.username = username;
		this.nickname = nickname;
		this.registtime = registtime;
		this.email = email;
		this.password = password;
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

}