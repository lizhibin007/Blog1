package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * expert
 * 
 * @author
 */
@Data
public class Expert implements Serializable {
	private Integer categoryid;

	private Long userid;

	private Byte handle;

	private static final long serialVersionUID = 1L;

	public Expert(Integer categoryid, Long userid, Byte handle) {
		this.categoryid = categoryid;
		this.userid = userid;
		this.handle = handle;
	}

	public Expert(Integer categoryid, Long userid) {
		this.categoryid = categoryid;
		this.userid = userid;
	}

	public Expert() {
	}
}