package cn.ncucode.blog.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * question
 * @author 
 */
@Data
public class Question implements Serializable {
    /**
     * 问题编号
     */
    private Long questionid;

    /**
     * 用户id
     */
    private Long userid;

    /**
     * 提出时间
     */
    private Date asktime;

    /**
     * 浏览量
     */
    private Integer lookcounts;

    /**
     * 点赞数
     */
    private Integer subscribecounts;

    /**
     * 回答数
     */
    private Integer replycounts;

    /**
     * 悬赏积分
     */
    private Integer scores;

    /**
     * 内容
     */
    private String content;

    /**
     * 标题
     */
    private String title;

    /**
     * 类别ID，资源对应的类别，管理员定义
     */
    private Integer categoryid;

    private static final long serialVersionUID = 1L;
}