package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * adopt
 * @author 
 */
@Data
public class AdoptKey implements Serializable {
    /**
     * 问题编号
     */
    private Long qsid;

    /**
     * 回答id
     */
    private Long replyid;

    private static final long serialVersionUID = 1L;
}