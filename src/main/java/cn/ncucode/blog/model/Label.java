package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * label
 * @author 
 */
@Data
public class Label implements Serializable {
    private Integer lableid;

    private Long blogname;

    private static final long serialVersionUID = 1L;
}