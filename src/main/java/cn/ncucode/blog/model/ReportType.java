package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * report_type
 * @author 
 */
@Data
public class ReportType implements Serializable {
    /**
     * 类型id
     */
    private Integer typeid;

    /**
     * 举报类型 有内容抄袭、政治相关等
     */
    private String type;

    /**
     * 对该举报类型的一些详细描述，方便用户选择
     */
    private String tpdescribe;

    private static final long serialVersionUID = 1L;
}