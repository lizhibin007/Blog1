package cn.ncucode.blog.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * private_mail
 * @author 
 */
@Data
public class PrivateMail implements Serializable {
    /**
     * 私信编号
     */
    private Long mailid;

    /**
     * 发送者id
     */
    private Long senderid;

    /**
     * 接收者id
     */
    private Long rcverid;

    /**
     * 私信内容
     */
    private String content;

    /**
     * 发送时间
     */
    private Date sendtime;

    /**
     * 是否已读,0未读 1已读
     */
    private Byte readen;

    private static final long serialVersionUID = 1L;
}