package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * qs_collection
 * @author 
 */
@Data
public class QsCollectionKey implements Serializable {
    /**
     * 问题编号
     */
    private Long qsid;

    /**
     * 用户id
     */
    private Long userid;

    private static final long serialVersionUID = 1L;
}