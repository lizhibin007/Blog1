package cn.ncucode.blog.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * report
 * @author 
 */
@Data
public class Report implements Serializable {
    /**
     * 举报主键
     */
    private Long reportid;

    /**
     * 举报时间
     */
    private Date reporttime;

    /**
     * 用于标志管理员的一些举报处理
     */
    private Byte handled;

    /**
     * 可选，即用户可以不填
     */
    private String reason;

    /**
     * 可以是问题、博客和资源,分别对应1、2、3
     */
    private Byte objtype;

    /**
     * 对应对象的id
     */
    private Long objid;

    /**
     * 举报者id
     */
    private Long userid;

    private static final long serialVersionUID = 1L;
}