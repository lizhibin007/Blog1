package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * label_blog
 * @author 
 */
@Data
public class LabelBlogKey implements Serializable {
    /**
     * 博客id
     */
    private Long blogid;

    /**
     * 标签id
     */
    private Integer labelid;

    private static final long serialVersionUID = 1L;
}