package cn.ncucode.blog.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * comment
 * @author 
 */
@Data
public class Comment implements Serializable {
    private Long commentid;

    private Date time;

    private Long blogid;

    private Long userid;

    private String content;

    private static final long serialVersionUID = 1L;
}