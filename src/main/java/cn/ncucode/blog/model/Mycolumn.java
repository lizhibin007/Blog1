package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * mycolumn
 * @author 
 */
@Data
public class Mycolumn implements Serializable {
    private Long columnid;

    private Long ownerid;

    private String name;

    private static final long serialVersionUID = 1L;
}