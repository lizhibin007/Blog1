package cn.ncucode.blog.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * category
 *
 * @author
 */
@Data
@AllArgsConstructor  //添加有参构造
@NoArgsConstructor
public class Category implements Serializable {
	private Integer categoryid;

	private String name;

	private static final long serialVersionUID = 1L;

    public Category(Integer categoryid) {
        this.categoryid = categoryid;
        this.name = "空";
    }
}