package cn.ncucode.blog.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * reply
 * @author 
 */
@Data
public class Reply implements Serializable {
    /**
     * 回答编号
     */
    private Long replyid;

    /**
     * 问题编号
     */
    private Long questionid;

    /**
     * 回答者ID
     */
    private Long replyerid;

    /**
     * 回答时间
     */
    private Date time;

    /**
     * 回答内容
     */
    private String content;

    private static final long serialVersionUID = 1L;
}