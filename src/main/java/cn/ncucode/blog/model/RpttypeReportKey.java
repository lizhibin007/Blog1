package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * rpttype_report
 * @author 
 */
@Data
public class RpttypeReportKey implements Serializable {
    /**
     * 举报id
     */
    private Long reportid;

    /**
     * 举报类型id
     */
    private Integer typeid;

    private static final long serialVersionUID = 1L;
}