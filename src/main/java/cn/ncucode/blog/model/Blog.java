package cn.ncucode.blog.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * blog
 * 
 * @author
 */
@Data
public class Blog implements Serializable {
	private Long blogid;

	private Date createtime;

	private Long authorid;

	private Integer look;

	private String title;

	private String content;

	private Byte allowcomment;

	private Long columnid;

	private Integer categoryid;

	private static final long serialVersionUID = 1L;
}