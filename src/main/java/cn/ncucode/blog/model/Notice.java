package cn.ncucode.blog.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * notice
 * @author 
 */
@Data
public class Notice implements Serializable {
    /**
     * 通知id
     */
    private Long noticeid;

    /**
     * 通知者id（实质是系统通知，通知者引发）
     */
    private Long infomid;

    /**
     * 被通知者id
     */
    private Long rcverid;

    /**
     * 通知类型,分为点赞通知、评论通知、系统通知等
     */
    private Byte type;

    /**
     * 通知内容,可以包含一些链接等
     */
    private String content;

    /**
     * 是否已读, 0未读 1已读
     */
    private Byte readen;

    /**
     * 发生时间
     */
    private Date happentime;

    private static final long serialVersionUID = 1L;
}