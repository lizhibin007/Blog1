package cn.ncucode.blog.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Resources {
    private long resourcesId;
    private User uploader;  //上传人   数据库中存储的是上传者的user id
    private Timestamp uploadTime;  //将java中的date转换成毫秒 并传入Timestamp构造函数里 返回timestamp
    private String resName;
    private int scores = 0;  //默认为0  下同
    private int download = 0;
    private double size = 0;  // 单位mb
    private String path;
    private String dsb;  //资源描述
    private Category category; //所属分类
    private String type; // rar/zip/txt/pdf/doc/docx /exe ?
}

