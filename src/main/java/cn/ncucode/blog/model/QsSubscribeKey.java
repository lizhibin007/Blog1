package cn.ncucode.blog.model;

import java.io.Serializable;
import lombok.Data;

/**
 * qs_subscribe
 * @author 
 */
@Data
public class QsSubscribeKey implements Serializable {
    /**
     * 问题编号
     */
    private Long qsid;

    /**
     * 用户id
     */
    private Long userid;

    private static final long serialVersionUID = 1L;
}